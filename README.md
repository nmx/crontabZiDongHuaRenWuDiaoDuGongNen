#### crontab自动化任务调度
> 支持守护进程方式,文件里面涉及到的"生成cron的网络地址"该地址的主要功能就是提供一个规范的cron格式的内容,内容可以根据业务动态生成也可以配置成静态文件,shell、cron目录可以根据需要自行调整位置。

#### shell目录：
1. check_process.sh    进程检测代码,用于进程守护
2. cron_diff.sh        文件比对,发现文件不同,重写当前cron文件
3. cron_merge.sh       文件合并
4. curl_daemon.sh      curl常驻进程,用于实时要求高的处理
5. curl_exec.sh        curl进程执行
6. multi_process.sh    curl多进程执行
7. process_daemon.sh   curl多进程执行常驻进程,用户实时多进程处理

#### cron目录:
1. all.cron            当前执行的cron文件
2. timer_diff.con      定时比对cron代码

#### 交流微信
![输入图片说明](http://src.leju.com/imp/imp/deal/08/23/a/fd7c92be6f78762ecf8d92f526e_p47_mk47.png "在这里输入图片标题")