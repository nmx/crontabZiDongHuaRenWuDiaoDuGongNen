#!/bin/bash
#开始时间
begin=$(date +%s)
url=$1
num=$2
for ((i=0; i<$num;))
do
        running=`ps -ef | grep $url | grep -v multi_thread.sh | grep -v grep | wc -l`
        #echo $running
        if [ $running -lt $num ]; then
                curl "$url"  >/dev/null 2>&1 &
        else
                echo "Thread num $running";
                break
        fi
        i=$(expr $i + 1)
done
#wait
#结束时间
end=$(date +%s)
spend=$(expr $end - $begin)
echo "finish"