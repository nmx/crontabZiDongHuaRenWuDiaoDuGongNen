#!/bin/bash
url=$1
pid=`ps -ef|grep "curl_daemon.sh ${url}"|grep -v "grep curl_daemon"|awk '{print $2}'`
if [ $pid ]; then
  echo "kill -9 ${pid}"
  kill -9 $pid
fi
curl "${url}"